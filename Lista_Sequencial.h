#define MAX 10

struct aluno{
	
	int matricula;
	char nome[30];
	float n1,n2,n3;
};

typedef struct lista Lista;

//CRIANDO UMA LISTA
Lista* cria_lista();


// DESCOBRIR O TAMANHO DE UMA LISTA

int tamanho_lista(Lista* l1);

// INSERINDO UM ELEMENTO NO FINAL DA LISTA
int insere_lista_final(Lista* li, struct aluno al);

// BUSCAR (ACESSAR) UM NÓ NA K-ESIMA POSIÇÃO

int busca_lista_posicao(Lista* li,int pos,struct aluno *al);