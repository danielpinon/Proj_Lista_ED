#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "Lista_Sequencial.h"

int main() {

Lista* li;

//ETAPA 01 - CRIANDO UMA LISTA

li = cria_lista();

int tamanho;

tamanho = tamanho_lista(li);

printf("tamanho da lista no momento: %d \n",tamanho);

int retorno;

printf("pressione enter para seguir...");
system("pause");


// ETAPA 02 - CRIANDO NÓS NA LISAT
	// INSERIR O CONTEÚDO DO PRIMEIRO NÓ DA LISTA
	struct aluno al;
	al.matricula = 19;
	strcpy(al.nome,"Alberto");
	al.n1 = 7.5;
	al.n2 = 6.0;
	al.n3 = 8.5;
	
	retorno = insere_lista_final(li, al);
	
	if(retorno == 1)
		printf("Foi inserido o 1o No\n");
	else
		printf("Não foi inserido o 1o No\n");

// INSERIR O CONTEÚDO DO PRIMEIRO NÓ DA LISTA
	
	al.matricula = 27;
	strcpy(al.nome,"Alberto");
	al.n1 = 5.0;
	al.n2 = 10.0;
	al.n3 = 9.0;
	
	retorno = insere_lista_final(li, al);
	
	if(retorno == 1)
		printf("Foi inserido o 2o No\n");
	else
		printf("Não foi inserido o 2o No\n");

// INSERIR O CONTEÚDO DO PRIMEIRO NÓ DA LISTA
	
	al.matricula = 35;
	strcpy(al.nome,"Alvaro");
	al.n1 = 3.0;
	al.n2 = 6.0;
	al.n3 = 8.0;
	
	retorno = insere_lista_final(li, al);
	
	if(retorno == 1)
		printf("Foi inserido o 3o No\n");
	else
		printf("Não foi inserido o 3o No\n");
		
// INSERIR O CONTEÚDO DO PRIMEIRO NÓ DA LISTA

	al.matricula = 43;
	strcpy(al.nome,"Berenice");
	al.n1 = 8.0;
	al.n2 = 2.0;
	al.n3 = 10.0;
	
	retorno = insere_lista_final(li, al);
	
	if(retorno == 1)
		printf("Foi inserido o 4o No\n");
	else
		printf("Não foi inserido o 4o No\n");

// INSERIR O CONTEÚDO DO PRIMEIRO NÓ DA LISTA
	
	al.matricula = 51;
	strcpy(al.nome,"Dagoberto");
	al.n1 = 0.5;
	al.n2 = 5.0;
	al.n3 = 6.8;
	
	retorno = insere_lista_final(li, al);
	
	if(retorno == 1)
		printf("Foi inserido o 5o No\n");
	else
		printf("Não foi inserido o 5o No\n");

tamanho = tamanho_lista(li);
printf("\n Tamanho da lista no momento: %d \n",tamanho);

printf("Pressione Enter para seguir...");
system("pause");


// ETAPA 03 - CONSULTANDO TODOS OS NÓS DA LISTA

printf("\nLista de alunos:\n");

int k; //DEFININDO O INCREMENTADOR PARA A POSIÇÃO
for (k=1; k<=tamanho;k++) {
	retorno = busca_lista_posicao(li,k,&al);
	
	if(retorno==1)
	  printf("Aluno(a) %d: %d %d %s %f %f %f \n",k,al.matricula,al.nome,al.n1,al.n2,al.n3);
	}
printf("Pressione Enter para seguir..");
system("pause");

return 0;


}