#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "Lista_Sequencial.h"


//Definição do tipo lista

struct lista{
	
	int qtd; // quantidade de elementos na lista
	struct aluno dados[MAX];

};

//CRIANDO UMA LISTA

Lista* cria_lista() {
	
	Lista *li;
	li = (Lista*) malloc(sizeof(Lista));
	
	if(li != NULL)
		li->qtd = 0;
	return li;
}



//DESCOBRIR O TAMANHO DA LISTA

int tamanho_lista(Lista* li){
	
	if(li == NULL)
		return -1;
	else
		return li->qtd;
}

// Inserindo um Elemento no final da lista

int insere_lista_final(Lista* li, struct aluno al){
	if(li == NULL)
		return 0;
	if(li->qtd == MAX)//Lista cheia
		return 0;
	li->dados[li->qtd] = al;
	li->qtd++;
	return 1;
}


// BUSCAR (ACESSAR) UM NÓ NA K-ESIMA POSIÇÃO

int busca_lista_posicao(Lista* li,int k,struct aluno *al){
	if (li == NULL)
		return 0;
	if(k <= 0 || k > li->qtd)
		return 0;
		
	*al = li->dados[k-1];// [k-1] PORQUE 1A POSIÇÃO É ZERO
	return 1;
}